"""Test cases for rcdefinition __main__ module."""
import unittest
from unittest import mock

from click.testing import CliRunner

from rcdefinition.__main__ import cli
from rcdefinition.__main__ import main


class TestRCDefinition(unittest.TestCase):
    """Test cases for rcdefinition __main__ module."""

    @mock.patch('builtins.print')
    def test_provision_api_main(self, mock_print):
        """Test that main click api (provision) works."""
        runner = CliRunner()

        response = runner.invoke(cli, ['triggervars', 'print'])
        self.assertEqual(response.exit_code, 0)

        mock_print.assert_called()

    @mock.patch('rcdefinition.__main__.cli')
    def test_main_method(self, mock_cli):
        # pylint: disable=no-self-use
        """Ensure main works and calls cli."""
        main()
        mock_cli.assert_called()
